/* eslint-disable react-hooks/exhaustive-deps */
import { userStore } from '@/store/state'
import { BaseLayoutProps } from '@/types/layouts'
import { MenuData } from '@/utils/data/menuLayoutData'
import { Layout, Menu } from 'antd'
import { useRouter } from 'next/router'
import React, { useEffect, useState } from 'react'
import { useRecoilValue, useSetRecoilState } from 'recoil'

const { Header, Footer } = Layout

export default function BaseLayout({ children }: BaseLayoutProps) {
	const [menuState, setMenuState] = useState() as any
	const userState = useRecoilValue(userStore)
	const setUserState = useSetRecoilState(userStore)
	const router = useRouter()
	const renderMenu = MenuData?.filter((item: any) => item.key !== 'login')

	useEffect(() => {
		setMenuState(
			userState?.admin
				? renderMenu &&
						renderMenu.concat(
							{
								label: (
									<span
										onClick={(e) => {
											e.preventDefault()
											router.push('/admin')
										}}
									>
										Admin
									</span>
								),
								key: 'admin',
							},
							{
								label: (
									<span
										onClick={() => {
											if (userState.admin) {
												router.push('/')
											}
											setUserState({
												id: '',
												username: '',
												nickname: '',
												email: '',
												admin: false,
											})
										}}
									>
										Logout
									</span>
								),
								key: 'logout',
							},
						)
				: userState?.id
				? renderMenu &&
				  renderMenu.concat({
						label: (
							<span
								onClick={() => {
									if (userState.admin) {
										router.push('/')
									}
									setUserState({
										id: '',
										username: '',
										nickname: '',
										email: '',
										admin: false,
									})
								}}
							>
								Logout
							</span>
						),
						key: 'logout',
				  })
				: MenuData,
		)
	}, [userState])

	return (
		<Layout id="layout" className="flex flex-col min-h-screen space-y-10">
			<Header>
				<div className="container">
					<Menu
						theme="dark"
						mode="horizontal"
						defaultSelectedKeys={['2']}
						items={menuState}
					/>
				</div>
			</Header>
			{children}
			<Footer className="text-center bg-[#001529] text-white">
				Ant Design ©2023 Created by Ant UED
			</Footer>
		</Layout>
	)
}
