/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @next/next/no-img-element */

import React, { useEffect, useState } from 'react'
import { Button, Form, Input, InputNumber, Modal, Select, message } from 'antd'
import { I_PropsModalCreate } from '@/types/models/products'
import { addOne } from '@/api/ProductsAPI'

const ModalCreate: React.FC<I_PropsModalCreate> = ({
	isOpenCreate,
	handleCancel,
	fetchList,
}) => {
	const { Option } = Select
	const [form] = Form.useForm()
	const [isOwn, setIsOwn] = useState<boolean>(false)

	const onFinish = async (value: object) => {
		try {
			const response = await addOne(value)
			if (response) {
				message.success('Add new cat success!')
				fetchList()
				handleCancel()
			}
		} catch (error) {
			message.error('Add new cat failed!')
		}
	}

	const handleIsOwnChange = (value: boolean) => {
		setIsOwn(value)
	}

	useEffect(() => {
		if (isOpenCreate) {
			form.resetFields()
		}
		setIsOwn(false)
	}, [isOpenCreate])

	return (
		<Modal
			title={'ADD NEW CAT'}
			open={isOpenCreate}
			onCancel={handleCancel}
			className="modalCreate"
		>
			<Form
				form={form}
				name="control-hooks"
				onFinish={onFinish}
				className="mt-4"
				style={{ maxWidth: 600 }}
			>
				<Form.Item name="name" label="Name" rules={[{ required: true }]}>
					<Input maxLength={32} />
				</Form.Item>
				<Form.Item
					name="nickname"
					label="Nickname"
					rules={[{ required: true }]}
				>
					<Input maxLength={32} />
				</Form.Item>
				<Form.Item name="gender" label="Gender" rules={[{ required: true }]}>
					<Select placeholder="Select gender" allowClear>
						<Option value="male">male</Option>
						<Option value="female">female</Option>
					</Select>
				</Form.Item>
				<Form.Item name="age" label="Age" rules={[{ required: true }]}>
					<InputNumber maxLength={2} />
				</Form.Item>
				<Form.Item name="price" label="Price" rules={[{ required: true }]}>
					<InputNumber maxLength={6} />
				</Form.Item>
				<Form.Item name="image" label="Image" rules={[{ required: true }]}>
					<Input />
				</Form.Item>
				<Form.Item name="isOwn" label="Is Own" rules={[{ required: true }]}>
					<Select placeholder="Is Own?" allowClear onChange={handleIsOwnChange}>
						<Option value={true}>Yes</Option>
						<Option value={false}>No</Option>
					</Select>
				</Form.Item>
				{isOwn ? (
					<Form.Item name="owner" label="Owner">
						<Input maxLength={32} />
					</Form.Item>
				) : (
					<></>
				)}
				<Form.Item>
					<Button type="primary" htmlType="submit">
						Create
					</Button>
				</Form.Item>
			</Form>
		</Modal>
	)
}

export default ModalCreate
