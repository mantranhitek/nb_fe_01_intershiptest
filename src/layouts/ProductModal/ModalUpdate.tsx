/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @next/next/no-img-element */

import React, { useEffect, useState } from 'react'
import { Button, Form, Input, InputNumber, Modal, Select, message } from 'antd'
import { I_PropsModalUpdate } from '@/types/models/products'
import { updateOne } from '@/api/ProductsAPI'

const ModalUpdate: React.FC<I_PropsModalUpdate> = ({
	isOpenUpdate,
	productItem,
	handleCancel,
	fetchList,
}) => {
	const { Option } = Select
	const [form] = Form.useForm()
	const [isOwn, setIsOwn] = useState<boolean>(false)

	const onFinish = async (value: object) => {
		try {
			if (productItem) {
				const response = await updateOne(productItem.id, value)
				if (response) {
					message.success('Update cat information success!')
					fetchList()
					handleCancel()
				}
			}
		} catch (error) {
			message.error('Update cat information failed!')
		}
	}

	const handleIsOwnChange = (value: boolean) => {
		setIsOwn(value)
	}

	useEffect(() => {
		if (productItem) {
			form.setFieldsValue({
				name: productItem.name,
				nickname: productItem.nickname,
				gender: productItem.gender,
				age: productItem.age,
				isOwn: productItem.isOwn,
				owner: productItem.owner,
				image: productItem.image,
				price: productItem.price,
			})

			setIsOwn(Boolean(productItem.isOwn))
		}
	}, [productItem])

	return (
		<Modal
			title={'UPDATE INFORMATION'}
			open={isOpenUpdate}
			onCancel={handleCancel}
			className="modalUpdate"
		>
			<Form
				form={form}
				name="control-hooks"
				onFinish={onFinish}
				className="mt-4"
				style={{ maxWidth: 600 }}
			>
				<Form.Item name="name" label="Name" rules={[{ required: true }]}>
					<Input maxLength={32} />
				</Form.Item>
				<Form.Item
					name="nickname"
					label="Nickname"
					rules={[{ required: true }]}
				>
					<Input maxLength={32} />
				</Form.Item>
				<Form.Item name="gender" label="Gender" rules={[{ required: true }]}>
					<Select placeholder="Select gender" allowClear>
						<Option value="male">male</Option>
						<Option value="female">female</Option>
					</Select>
				</Form.Item>
				<Form.Item name="age" label="Age" rules={[{ required: true }]}>
					<InputNumber maxLength={2} />
				</Form.Item>
				<Form.Item name="price" label="Price" rules={[{ required: true }]}>
					<InputNumber maxLength={6} />
				</Form.Item>
				<Form.Item name="image" label="Image" rules={[{ required: true }]}>
					<Input />
				</Form.Item>
				<Form.Item name="isOwn" label="Is Own" rules={[{ required: true }]}>
					<Select placeholder="Is Own?" allowClear onChange={handleIsOwnChange}>
						<Option value={true}>Yes</Option>
						<Option value={false}>No</Option>
					</Select>
				</Form.Item>
				{isOwn ? (
					<Form.Item name="owner" label="Owner">
						<Input maxLength={32} />
					</Form.Item>
				) : (
					<></>
				)}
				<Form.Item>
					<Button type="primary" htmlType="submit">
						Update
					</Button>
				</Form.Item>
			</Form>
		</Modal>
	)
}

export default ModalUpdate
