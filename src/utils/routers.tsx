export const ROUTES = {
	PRODUCTS: '/products',
	PRODUCT: '/product/:id',
	LOGIN: '/login',
	REGISTER: '/register',
}

export const PRODUCTS_API_ROUTES = {
	PRODUCTS: '/cats',
}

export const AUTH_API_ROUTES = {
	AUTH_REGISTER: '/register',
	AUTH_LOGIN: '/login',
}
