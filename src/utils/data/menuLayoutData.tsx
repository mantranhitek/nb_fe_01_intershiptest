import { MenuProps } from 'antd'
import Link from 'next/link'
import { ROUTES } from '../routers'

export const MenuData: MenuProps['items'] = [
	{
		label: <Link href={ROUTES.PRODUCTS}>Products</Link>,
		key: 'product',
	},
	{
		label: <Link href={ROUTES.LOGIN}>Login</Link>,
		key: 'login',
	},
]
