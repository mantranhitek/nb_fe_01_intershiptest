import axios from '@/axios'
import { APIGetParams, convertParams, exportResults } from '@/utils/api'
import { PRODUCTS_API_ROUTES } from '@/utils/routers'

export const getList = async (params: APIGetParams) =>
	exportResults(
		await axios.get(PRODUCTS_API_ROUTES.PRODUCTS, {
			params: convertParams(params),
		}),
	)

export const getOne = async (id: string) =>
	exportResults(await axios.get(`${PRODUCTS_API_ROUTES.PRODUCTS}/${id}`))

export const addOne = async (body: object) =>
	exportResults(await axios.post(PRODUCTS_API_ROUTES.PRODUCTS, body))

export const updateOne = async (id: string, body: object) =>
	exportResults(
		await axios.patch(`${PRODUCTS_API_ROUTES.PRODUCTS}/${id}`, body),
	)

export const deleteOne = async (id: string) =>
	exportResults(await axios.delete(`${PRODUCTS_API_ROUTES.PRODUCTS}/${id}`))
