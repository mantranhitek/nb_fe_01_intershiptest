/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @next/next/no-img-element */

import BaseLayout from '@/layouts/BaseLayout'
import ModalUpdate from '@/layouts/ProductModal/ModalUpdate'
import { userStore } from '@/store/state'
import { I_CatProduct, I_Pagination } from '@/types/models/products'
import { Button, Space, Table, message } from 'antd'
import type { ColumnsType } from 'antd/es/table'
import Head from 'next/head'
import { useRouter } from 'next/router'
import React, { useEffect, useState } from 'react'
import { useRecoilValue } from 'recoil'
import { deleteOne, getList } from '../../api/ProductsAPI'
import ModalCreate from '../../layouts/ProductModal/ModalCreate'

const Products: React.FC = () => {
	// Var
	const [data, setData] = useState<Array<I_CatProduct>>([])
	const [isOpenCreate, setIsOpenCreate] = useState<boolean>(false)
	const [isOpenUpdate, setIsOpenUpdate] = useState<boolean>(false)
	const [productItem, setProductItem] = useState<I_CatProduct>()
	const [pagination, setPagination] = useState<I_Pagination>({
		pageSize: 5,
		current: 1,
	})
	const user = useRecoilValue(userStore)
	const router = useRouter()

	const columns: ColumnsType<I_CatProduct> = [
		{
			title: '#',
			dataIndex: 'no',
			key: 'no',
		},
		{
			title: 'Name',
			dataIndex: 'name',
			key: 'name',
		},
		{
			title: 'Image',
			dataIndex: 'image',
			key: 'image',
			render: (param1: string) => {
				return (
					<img
						className="object-cover object-center rounded"
						style={{ width: '80px', height: '80px' }}
						src={param1}
						alt="img"
					/>
				)
			},
		},
		{
			title: 'Price',
			dataIndex: 'price',
			key: 'price',
			render: (param1: number) => {
				return <p>${param1}.00</p>
			},
		},
		{
			title: 'Gender',
			dataIndex: 'gender',
			key: 'gender',
			render: (param1: string) => {
				if (param1) {
					const newParam1 = param1.toUpperCase()
					return <p>{newParam1}</p>
				}
			},
		},
		{
			title: 'Age',
			dataIndex: 'age',
			key: 'age',
		},
		{
			title: 'Owned',
			key: 'isOwn',
			dataIndex: 'isOwn',
			render: (param1: boolean) => {
				return (
					<>
						{param1 ? (
							<p className="text-green-600">Yes</p>
						) : (
							<p className="text-red-600">No</p>
						)}
					</>
				)
			},
		},
		{
			title: 'Action',
			key: 'action',
			render: (_, param2: I_CatProduct) => (
				<Space size="middle">
					<Button
						onClick={() => {
							handleNavigateProductDetail(param2.id)
						}}
						className="bg-blue-500 text-white"
					>
						Detail
					</Button>
					<Button
						onClick={() => {
							showModalUpdate(param2)
						}}
						className="bg-orange-500 text-white"
					>
						Update
					</Button>
					<Button
						onClick={() => {
							handleDelete(param2.id)
						}}
						className="bg-red-500 text-white"
					>
						Delete
					</Button>
				</Space>
			),
		},
	]

	// Function
	const fetchList = async () => {
		const response = await getList({
			_limit: pagination.pageSize,
			_page: pagination.current,
		})
		if (response.data) {
			const newArr: Array<I_CatProduct> = []

			response.data.map((item: I_CatProduct, index: number) => {
				const newItem: I_CatProduct = {
					no: index + 1 + pagination.pageSize * (pagination.current - 1),
					id: item.id,
					name: item.name,
					nickname: item.nickname,
					gender: item.gender,
					age: item.age,
					isOwn: item.isOwn,
					owner: item.owner,
					image: item.image,
					price: item.price,
					key: item.id,
				}
				newArr.push(newItem)
			})

			setData(newArr)
			setPagination((prevPagination) => ({
				...prevPagination,
				total: response.pagination._totalRows,
			}))
		}
	}

	const handleTableChange = (pagination: any) => {
		setPagination(pagination)
	}

	const handleDelete = async (id: string) => {
		const index: number = data.findIndex((item: I_CatProduct) => {
			return item.id === id
		})

		if (index != -1) {
			try {
				const id = data[index].id
				const response = await deleteOne(id)
				if (response) {
					message.success('Delete Success!')
					fetchList()
				}
			} catch (error: any) {
				message.error(error.response.data.message)
			}
		}
	}

	const handleCancel = () => {
		setIsOpenCreate(false)
		setIsOpenUpdate(false)
	}

	const handleNavigateProductDetail = (id: string): void => {
		router.push(`/products/${id}`)
	}

	const showModalCreate = () => {
		setIsOpenCreate(true)
	}

	const showModalUpdate = (data: I_CatProduct) => {
		setProductItem(data)
		setIsOpenUpdate(true)
	}

	useEffect(() => {
		if (user.admin) {
			fetchList()
		} else {
			router.push('/products')
		}
	}, [pagination.current])

	return (
		<>
			<Head>
				<title>Admin Page</title>
			</Head>
			<BaseLayout>
				{/* Main */}
				<div id="product" className="flex-grow">
					<div className="container space-y-4">
						<h2 className="text-center font-bold text-2xl sm:text-4xl animate-bounce">
							CAT STORE
						</h2>
						<Button
							onClick={showModalCreate}
							className="bg-green-600 text-white"
						>
							Add New
						</Button>
						<div className="max-w-full overflow-x-scroll text-xs sm:overflow-x-auto sm:text-base">
							<Table
								columns={columns}
								dataSource={data}
								pagination={pagination}
								onChange={handleTableChange}
								className="lg:border-4 lg:rounded-lg"
							/>
						</div>
					</div>
				</div>

				{/* Modal */}
				<ModalCreate
					isOpenCreate={isOpenCreate}
					handleCancel={handleCancel}
					fetchList={fetchList}
				/>
				<ModalUpdate
					isOpenUpdate={isOpenUpdate}
					productItem={productItem}
					handleCancel={handleCancel}
					fetchList={fetchList}
				/>
			</BaseLayout>
		</>
	)
}

export default Products
