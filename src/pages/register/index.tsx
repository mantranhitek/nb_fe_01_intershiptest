/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @next/next/no-img-element */

import { register } from '@/api/AuthAPI'
import BaseLayout from '@/layouts/BaseLayout'
import { userStore } from '@/store/state'
import { Button, Form, Input, Select, message } from 'antd'
import Head from 'next/head'
import { useRouter } from 'next/router'
import React, { useEffect } from 'react'
import { useSetRecoilState } from 'recoil'

const { Option } = Select

const Register: React.FC = () => {
	const [form] = Form.useForm()
	const router = useRouter()
	const setUserState = useSetRecoilState(userStore)

	const onFinish = async (values: any) => {
		try {
			const response = await register(values)
			if (response) {
				message.success('Register Success!')
				router.push('/login')
			}
		} catch (error: any) {
			message.error(error.response.data.message)
		}
	}

	useEffect(() => {
		setUserState({
			id: '',
			username: '',
			nickname: '',
			email: '',
			admin: false,
		})
	}, [])

	return (
		<>
			<Head>
				<title>Register</title>
			</Head>
			<BaseLayout>
				<div id="register" className="flex-grow">
					<div className="container">
						<h2 className="font-bold text-2xl sm:text-4xl mb-8">
							REGISTER PAGE
						</h2>
						<Form
							form={form}
							name="register"
							onFinish={onFinish}
							style={{ maxWidth: 600 }}
							scrollToFirstError
						>
							<Form.Item
								name="username"
								label="Username"
								rules={[
									{
										required: true,
										message: 'Please input your username!',
										whitespace: true,
									},
								]}
							>
								<Input />
							</Form.Item>

							<Form.Item
								name="password"
								label="Password"
								rules={[
									{
										required: true,
										message: 'Please input your password!',
									},
								]}
								hasFeedback
							>
								<Input.Password />
							</Form.Item>

							<Form.Item
								name="confirm"
								label="Confirm Password"
								dependencies={['password']}
								hasFeedback
								rules={[
									{
										required: true,
										message: 'Please confirm your password!',
									},
									({ getFieldValue }) => ({
										validator(_, value) {
											if (!value || getFieldValue('password') === value) {
												return Promise.resolve()
											}
											return Promise.reject(
												new Error(
													'The new password that you entered do not match!',
												),
											)
										},
									}),
								]}
							>
								<Input.Password />
							</Form.Item>

							<Form.Item
								name="nickname"
								label="Nickname"
								tooltip="What do you want others to call you?"
								rules={[
									{
										required: true,
										message: 'Please input your nickname!',
										whitespace: true,
									},
								]}
							>
								<Input />
							</Form.Item>

							<Form.Item
								name="email"
								label="E-mail"
								rules={[
									{
										type: 'email',
										message: 'The input is not valid E-mail!',
									},
									{
										required: true,
										message: 'Please input your E-mail!',
									},
								]}
							>
								<Input />
							</Form.Item>

							<Form.Item
								name="admin"
								label="Admin?"
								rules={[{ required: true }]}
							>
								<Select placeholder="Are you admin?" allowClear>
									<Option value={true}>Yes</Option>
									<Option value={false}>No</Option>
								</Select>
							</Form.Item>

							<Form.Item>
								<Button
									className="bg-green-600"
									type="primary"
									htmlType="submit"
								>
									Register
								</Button>
							</Form.Item>
						</Form>
					</div>
				</div>
			</BaseLayout>
		</>
	)
}

export default Register
