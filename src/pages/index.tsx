import Head from 'next/head'
import Products from './products'

export default function Home() {
	return (
		<>
			<Head>
				<title>Cat Store</title>
			</Head>
			<Products />
		</>
	)
}
