/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @next/next/no-img-element */

import { getOne, updateOne } from '@/api/ProductsAPI'
import BaseLayout from '@/layouts/BaseLayout'
import { userStore } from '@/store/state'
import { I_CatProduct } from '@/types/models/products'
import { Button, message } from 'antd'
import Head from 'next/head'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import { useRecoilValue } from 'recoil'

export default function ProductDetail() {
	const [productDetailItem, setProductDetailItem] = useState<I_CatProduct>()
	const user = useRecoilValue(userStore)
	const router = useRouter()
	const { id } = router.query

	const handleBuyNow = async () => {
		try {
			const data: I_CatProduct = {
				id: String(productDetailItem?.id),
				name: productDetailItem?.name,
				nickname: productDetailItem?.nickname,
				gender: productDetailItem?.gender,
				age: productDetailItem?.age,
				image: productDetailItem?.image,
				price: productDetailItem?.price,
				isOwn: true,
				owner: user?.nickname,
			}
			const response = await updateOne(String(id), data)
			if (response) {
				message.success('Buy cat success!')
				router.push('/products')
			}
		} catch (error) {
			message.error('Buy cat failed! Please try again!')
		}
	}

	const fetchLayout = async () => {
		try {
			if (id) {
				const response = await getOne(String(id))
				if (response) {
					setProductDetailItem(response)
				}
			}
		} catch (error: any) {
			message.error(error.response.data.message)
		}
	}

	useEffect(() => {
		fetchLayout()
	}, [id])

	return (
		<>
			<Head>
				<title>Product Detail</title>
			</Head>
			<BaseLayout>
				<div className="productDetail flex-grow space-y-10">
					<h2 className="text-center font-bold text-2xl sm:text-4xl">
						PRODUCT DETAIL
					</h2>
					<div className="container sm:flex sm:space-y-0 space-y-4">
						<div className="w-full sm:w-2/5 mr-20">
							<img
								className="rounded-lg"
								src={productDetailItem?.image}
								alt={productDetailItem?.nickname}
							/>
						</div>
						<div className="space-y-4">
							{/* Title */}
							{productDetailItem?.isOwn ? (
								<h3 className="font-semibold uppercase text-xl text-red-600">
									{productDetailItem?.name}
								</h3>
							) : (
								<h3 className="font-semibold uppercase text-xl text-green-600">
									{productDetailItem?.name}
								</h3>
							)}

							{/* Content */}
							<div className="space-y-2">
								<p>
									<span className="font-medium">Nickname:</span>{' '}
									{productDetailItem?.nickname}
								</p>
								<p>
									<span className="font-medium">Gender:</span>{' '}
									{productDetailItem?.gender}
								</p>
								<p>
									<span className="font-medium">Age:</span>{' '}
									{productDetailItem?.age}
								</p>
								<p>
									<span className="font-medium">Available:</span>{' '}
									{productDetailItem?.isOwn ? (
										<span className="text-red-600 font-medium">No</span>
									) : (
										<span className="text-green-600 font-medium">Yes</span>
									)}
								</p>
								{productDetailItem?.isOwn ? (
									<p>
										<span className="font-medium">Owner:</span>{' '}
										{productDetailItem?.owner}
									</p>
								) : (
									<></>
								)}
							</div>

							{/* Action */}
							<div className="flex space-x-4	">
								<p className="bg-stone-600 text-white w-24 text-center h-10 leading-10 rounded-lg font-semibold">
									${productDetailItem?.price}.00
								</p>
								{productDetailItem?.isOwn ? (
									<Button
										className="bg-red-500 text-white w-24 h-10 rounded-lg font-semibold"
										disabled
									>
										SOLD OUT
									</Button>
								) : (
									<Button
										onClick={handleBuyNow}
										className="bg-green-500 hover:bg-green-600 text-white w-24 h-10 rounded-lg font-semibold"
									>
										BUY NOW
									</Button>
								)}
							</div>
						</div>
					</div>
				</div>
			</BaseLayout>
		</>
	)
}
