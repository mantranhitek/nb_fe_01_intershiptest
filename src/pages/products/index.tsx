/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @next/next/no-img-element */

import BaseLayout from '@/layouts/BaseLayout'
import { I_CatProduct, I_Pagination } from '@/types/models/products'
import { Button, Space, Table } from 'antd'
import type { ColumnsType } from 'antd/es/table'
import Head from 'next/head'
import { useRouter } from 'next/router'
import React, { useEffect, useState } from 'react'
import { getList } from '../../api/ProductsAPI'

const Products: React.FC = () => {
	// Var
	const [data, setData] = useState<Array<I_CatProduct>>([])
	const [pagination, setPagination] = useState<I_Pagination>({
		pageSize: 5,
		current: 1,
	})
	const router = useRouter()

	const columns: ColumnsType<I_CatProduct> = [
		{
			title: '#',
			dataIndex: 'no',
			key: 'no',
		},
		{
			title: 'Name',
			dataIndex: 'name',
			key: 'name',
		},
		{
			title: 'Image',
			dataIndex: 'image',
			key: 'image',
			render: (param1: string) => {
				return (
					<img
						className="object-cover object-center rounded"
						style={{ width: '80px', height: '80px' }}
						src={param1}
						alt="img"
					/>
				)
			},
		},
		{
			title: 'Price',
			dataIndex: 'price',
			key: 'price',
			render: (param1: number) => {
				return <p>${param1}.00</p>
			},
		},
		{
			title: 'Gender',
			dataIndex: 'gender',
			key: 'gender',
			render: (param1: string) => {
				if (param1) {
					const newParam1 = param1.toUpperCase()
					return <p>{newParam1}</p>
				}
			},
		},
		{
			title: 'Age',
			dataIndex: 'age',
			key: 'age',
		},
		{
			title: 'Owned',
			key: 'isOwn',
			dataIndex: 'isOwn',
			render: (param1: boolean) => {
				return (
					<>
						{param1 ? (
							<p className="text-green-600">Yes</p>
						) : (
							<p className="text-red-600">No</p>
						)}
					</>
				)
			},
		},
		{
			title: 'Action',
			key: 'action',
			render: (_, param2: I_CatProduct) => (
				<Space size="middle">
					<Button
						onClick={() => {
							handleNavigateProductDetail(param2.id)
						}}
						className="bg-blue-500 text-white"
					>
						Detail
					</Button>
				</Space>
			),
		},
	]

	// Function
	const fetchList = async () => {
		const response = await getList({
			_limit: pagination.pageSize,
			_page: pagination.current,
		})
		if (response.data) {
			const newArr: Array<I_CatProduct> = []

			response.data.map((item: I_CatProduct, index: number) => {
				const newItem: I_CatProduct = {
					no: index + 1 + pagination.pageSize * (pagination.current - 1),
					id: item.id,
					name: item.name,
					nickname: item.nickname,
					gender: item.gender,
					age: item.age,
					isOwn: item.isOwn,
					owner: item.owner,
					image: item.image,
					price: item.price,
					key: item.id,
				}
				newArr.push(newItem)
			})

			setData(newArr)
			setPagination((prevPagination) => ({
				...prevPagination,
				total: response.pagination._totalRows,
			}))
		}
	}

	const handleTableChange = (pagination: any) => {
		setPagination(pagination)
	}

	const handleNavigateProductDetail = (id: string): void => {
		router.push(`/products/${id}`)
	}

	useEffect(() => {
		fetchList()
	}, [pagination.current])

	return (
		<>
			<Head>
				<title>Cat Store</title>
			</Head>
			<BaseLayout>
				{/* Main */}
				<div id="product" className="flex-grow">
					<div className="container space-y-4">
						<h2 className="text-center font-bold text-2xl sm:text-4xl animate-bounce">
							CAT STORE
						</h2>
						<div className="max-w-full overflow-x-scroll text-xs sm:overflow-x-auto sm:text-base">
							<Table
								columns={columns}
								dataSource={data}
								pagination={pagination}
								onChange={handleTableChange}
								className="lg:border-4 lg:rounded-lg"
							/>
						</div>
					</div>
				</div>
			</BaseLayout>
		</>
	)
}

export default Products
