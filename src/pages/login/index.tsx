/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @next/next/no-img-element */

import React, { useEffect } from 'react'
import { Button, Checkbox, Form, Input, message } from 'antd'
import { login } from '@/api/AuthAPI'
import { useSetRecoilState } from 'recoil'
import { UserModal } from '@/types/models/user'
import { userStore } from '@/store/state'
import { useRouter } from 'next/router'
import BaseLayout from '@/layouts/BaseLayout'
import Head from 'next/head'

const Login: React.FC = () => {
	const setUser = useSetRecoilState<UserModal>(userStore)
	const router = useRouter()

	const onFinish = async (values: any) => {
		try {
			const response = await login(values)

			if (response) {
				if (response.data.admin) {
					message.success(`Hello, ADMIN ${response.data.nickname}!`)
				} else {
					message.success(`Hello, ${response.data.nickname}!`)
				}

				setUser(response.data)
				router.push('/')
			}
		} catch (error: any) {
			message.error(error.response.data.message)
		}
	}

	const handleNavigateRegister = (): void => {
		router.push('/register')
	}

	useEffect(() => {
		setUser({
			id: '',
			username: '',
			nickname: '',
			email: '',
			admin: false,
		})
	}, [])

	return (
		<>
			<Head>
				<title>Login</title>
			</Head>
			<BaseLayout>
				<div className="flex flex-col flex-grow justify-center items-center space-y-10">
					<h2 className="text-center font-bold text-2xl sm:text-4xl">
						LOGIN PAGE
					</h2>
					<div className="w-3/5 lg:w-2/5 h-80 sm:border-4 flex justify-center items-center sm:rounded-lg">
						<Form
							name="basic"
							labelCol={{ span: 8 }}
							wrapperCol={{ span: 16 }}
							style={{ maxWidth: 600 }}
							initialValues={{ remember: true }}
							onFinish={onFinish}
							autoComplete="off"
						>
							<Form.Item
								label="Username"
								name="username"
								rules={[
									{ required: true, message: 'Please input your username!' },
								]}
							>
								<Input />
							</Form.Item>

							<Form.Item
								label="Password"
								name="password"
								rules={[
									{ required: true, message: 'Please input your password!' },
								]}
							>
								<Input.Password />
							</Form.Item>

							<Form.Item
								name="remember"
								valuePropName="checked"
								wrapperCol={{ offset: 8, span: 16 }}
							>
								<Checkbox>Remember me</Checkbox>
							</Form.Item>

							<Form.Item wrapperCol={{ offset: 8, span: 16 }}>
								<Button
									className="bg-blue-600 mr-1 sm:mr-3 mb-2"
									type="primary"
									htmlType="submit"
								>
									Login
								</Button>
								<Button
									onClick={handleNavigateRegister}
									className="bg-green-600"
									type="primary"
								>
									Register
								</Button>
							</Form.Item>
						</Form>
					</div>
				</div>
			</BaseLayout>
		</>
	)
}

export default Login
