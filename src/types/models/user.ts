export type UserModal = {
	id: string
	username: string
	nickname: string
	email: string
	admin: boolean
}
