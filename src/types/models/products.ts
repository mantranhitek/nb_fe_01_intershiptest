export interface I_CatProduct {
	no?: number
	id: string
	name?: string
	nickname?: string
	gender?: string
	age?: number
	isOwn?: boolean
	owner?: string
	image?: string
	price?: number
	key?: string
}

export interface I_Pagination {
	pageSize: number
	current: number
	total?: number
}

export interface I_PropsModalUpdate {
	isOpenUpdate: boolean
	productItem: I_CatProduct | undefined
	handleCancel: () => void
	fetchList: () => void
}

export interface I_PropsModalCreate {
	isOpenCreate: boolean
	handleCancel: () => void
	fetchList: () => void
}
