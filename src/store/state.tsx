import { UserModal } from '@/types/models/user'
import { atom } from 'recoil'
import { recoilPersist } from 'recoil-persist'

const { persistAtom } = recoilPersist()

export const userStore = atom<UserModal>({
	key: 'userStore',
	default: {
		id: '',
		username: '',
		nickname: '',
		email: '',
		admin: false,
	},
	effects_UNSTABLE: [persistAtom],
})

// export const getNameOfUserStore = selector({
// 	key: 'nameStore',
// 	get: ({ get }) => {
// 		return get(userStore).nickname
// 	},
// })

// export const getTokenOfUSerStore = selector({
// 	key: 'tokenStore',
// 	get: ({ get }) => {
// 		return get(userStore).admin
// 	},
// })
