Deployed Website: https://mantran-intershiptest.vercel.app/

Next.js with TypeScript and ESLint
Starter code for a clean Next.js + TypeScript + ESLint project.

Get started

# (First) Setup & Enable API

- Step 1: Open file json_server.
- Step 2: Run "yarn start" to start API localhost:3000.

# (Second) Setup & Start Dev Server

- Step 1: Open file nb_fe_01_internshiptest.
- Step 2: Run "yarn" to install dependencies and enable husky.
- Step 3: Run "yarn dev" to start dev server at localhost:3001.

# Function

- Tạo API giả lập
- Trang đăng ký/đăng nhập, phân quyền user.
- Trang danh sách (bao gồm phân trang).
- Trang chi tiết.
- Thêm, xóa, sửa.
- Responsive.

# Features

- ESLint and Prettier are integrated with VSCode to fix and format code on save (you need eslint and prettier VSCode plugins).
- lint-staged: linting will only happen on staged files, not all file
  Latest Husky.
- TypeScript types are checked before each commit.

# Commit Code Example

          		FEATURE: "update feature 1.1",
    			INIT: "init feature 1.1",
    			BUILD: "build feature 1.1",
    			CHORE: "chore feature 1.1",
    			CI: "update feature 1.1",
    			DCS: "update feature 1.1",
    			FEAT: "update feature 1.1",
    			FIX: "update feature 1.1",
    			MIGRATION: "update feature 1.1",
    			PERF: "update feature 1.1",
    			REFACTOR: "update feature 1.1",
    			REVERT: "update feature 1.1",
    			STYLE: "update feature 1.1",
    			TEST: "update feature 1.1",
    			TRANSLATION: "update feature 1.1",
    			SECURITY: "update feature 1.1",
    			CHANGESET: "update feature 1.1",
